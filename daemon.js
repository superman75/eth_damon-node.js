
var assert = require('better-assert');
var db = require('./src/db');
var web3_client = require('./src/web3_client');
var querystring = require('querystring');
var https = require('https');

// this is the address of game site
var depositAddr;
var client = web3_client.web3;
var lastBlockNumber;
var lastBlockHash;

loopETHvsBTCRate();// get ETH / BTC Rate

startBlockLoop();

function processTransactionIds(txids, callback) 
{
    if (txids.length == 0) return callback('');

    var from=[], value=[], hashes=[];
    for(var i = 0; i < txids.length; i++) 
    {
        var userid, txinfo;
        try{
            txinfo = client.eth.getTransaction(txids[i]);
        }
        catch(e){
            console.log('cannot connect to the node accidentally.');
            continue;
        }
        if (txinfo == null)
            return callback('getTransaction call of hash value ' + txids[i] + ' returns null');
        if (txinfo.to == null)  continue;
        if (txinfo.to.toLowerCase() == depositAddr.toLowerCase()) {
            from.push( txinfo.from.toLowerCase() );
            value.push( parseInt(client.fromWei(txinfo.value, 'finney')) );
            hashes.push( txinfo.hash );
        }
    }
    if (from.length == 0) return callback('');
    for(var j = 0; j < from.length; j++)
    {
        db.checkUserId(from[j], value[j], hashes[j], function(err, res){
            return callback(err);
        });
    }
}

function startBlockLoop() {
    db.LoadDepositSrc(function(err, res){
        if (err) {
           console.loog('Cannnot load deposit source information.');
           setTimeout(startBlockLoop, 10000);
        }

        depositAddr = res.addr;

        db.getLastBlock(function (error, block) {
            if (error) {
                throw new Error('Unable to get initial last block: ', error);
            }

            lastBlockNumber = block.height;// block.height;
            lastBlockHash = block.hash;

            console.log('Initialized on Ethereum block: ', lastBlockNumber, ' with hash: ', lastBlockHash);

            blockLoop();
        });
    });
}

function scheduleBlockLoop() {
    setTimeout(blockLoop, 20000); // every 20s
}

function blockLoop() {
    // console.log("Last block hash from variable : " + client.eth.blockNumber);

    client.getBlockNumber(function (error, number) 
    {
        if (error) 
        {
            console.error('ETH: Unable to get block count' + error);
            return scheduleBlockLoop();
        }
        // var number = client.eth.syncing.highestBlock;
        console.log("\nLast block number : " + number);

        if (number === lastBlockNumber)
        {
            console.log('Block chain still ', number, '. No need to do anything');
            return scheduleBlockLoop();
        }

        client.getBlockHash(lastBlockNumber, function (err, hash) {
            if (err) {
                console.error('Could not get block hash, error: ' + err);
                return scheduleBlockLoop();
            }

            if (lastBlockHash !== hash) 
            {
                db.getBlock(lastBlockNumber - 1, function (err, block) {
                    if (err) {
                        console.error('ETH: ERROR: Unable to jump back ', err);
                        return scheduleBlockLoop();
                    }
                    --lastBlockNumber;
                    lastBlockHash = block.hash;
                    blockLoop();
                });
                return;
            }

            client.getBlockHash(lastBlockNumber+1, function (err, hash)
            {
                if (err) {
                    console.error('Unable to get block hash: ', lastBlockNumber + 1);
                    return scheduleBlockLoop();
                }

                console.log("Go to process block : hash : " + (lastBlockNumber+1) + "    " + hash);

                processBlock(hash, function (err)
                {
                    if (err) {
                        console.log('Unable to process block: ', hash, ' because: ', err);
                        return scheduleBlockLoop();
                    }
                    ++lastBlockNumber; // this part also should be considered like the former one. needs t po
                    lastBlockHash = hash;
                    db.insertBlock(lastBlockNumber, lastBlockHash, function (err) {
                        if (err) console.error('Danger, unable to save results in database...', err);
                        blockLoop();
                    });
                });
            });
        });
    });
}

function processBlock(hash, callback)
{
    // console.log('processBlock : ', hash);

    var start = new Date();
    
    client.eth.getBlock(hash, function (err, blockInfo) {
        if (err) {
            console.error('Unable to get the block information for: ', hash, ' got error: ', err);
            return callback(err);
        }
        var transactionIds = blockInfo.transactions;

        processTransactionIds(transactionIds, function (err) {
            if (err)
                console.log('Unable to process block (in ', (new Date() - start) / 1000, ' seconds)');
            else
                console.log('Processed ', transactionIds.length, ' transactions in ', (new Date() - start) / 1000, ' seconds');
            callback(err);
        })
    })
}

// https://cex.io/api/ticker/ETH/BTC
// get ETH / BTC Rate
function loopETHvsBTCRate()
{
    var optionsget = {
        host : 'cex.io', // here only the domain name
        // (no http/https !)
        port : 443,
        path : '/api/ticker/ETH/BTC', // the rest of the url with parameters if needed
        method : 'GET' // do GET
    };

    var reqGet = https.request(optionsget, function(res)
    {
        var body = '';
        res.on('data', function(chunk) {
            body += chunk;
        });

        res.on('end', function()
        {
            var fbResponse = JSON.parse(body);

            db.updateETHvsBTCRate(fbResponse.last, function(result)
            {
                if (result === 'success')
                {
                    console.log("ETH: ETH/BTC update success: " + fbResponse.last);
                }
                else
                {
                    console.log("ETH: ETH/BTC update error");
                }

                setTimeout(loopETHvsBTCRate, 20000);// update every 20s
            });
        });
    });

    reqGet.end();
    reqGet.on('error', function(e)
    {
        console.error(e);
    });
}

