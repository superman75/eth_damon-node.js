//postgresql API
var assert = require('better-assert');
var async = require('async');
var pg = require('pg');
var fs = require('fs');


var g_fEBRate = 0.11;

var depositAddresses = JSON.parse(fs.readFileSync('.addresses_eth.json', 'utf8'));
exports.depositAddresses = depositAddresses;
var databaseUrl = process.env.DATABASE_URL;
assert(databaseUrl);
var keys = Object.keys(depositAddresses);

// console.log('DATABASE_URL: ', databaseUrl);

pg.types.setTypeParser(20, function(val) { // parse int8 as an integer
    return val === null ? null : parseInt(val);
});

function refreshView() {

    query('REFRESH MATERIALIZED VIEW CONCURRENTLY leaderboard;', function(err)
    {
        if (err)
        {
            console.error('[INTERNAL_ERROR] unable to refresh leaderboard got: ', err);
        } else {
            console.log('leaderboard refreshed');
        }

        setTimeout(refreshView, 10 * 60 * 1000);
    });

}
setTimeout(refreshView, 1000); // schedule it so it comes after all the addresses are generated


// callback is called with (err, client, done)
function connect(callback) {
    return pg.connect(databaseUrl, callback);
}

function query(query, params, callback) {
    //thrid parameter is optional
    if (typeof params == 'function') {
        callback = params;
        params = [];
    }

    connect(function(err, client, done) {
        if (err) return callback(err);
        client.query(query, params, function(err, result) {
            done();
            if (err) {
                return callback(err);
            }
            callback(null, result);
        });
    });
}

// runner takes (client, callback)

// callback should be called with (err, data)
// client should not be used to commit, rollback or start a new transaction


// callback takes (err, data)

function getClient(runner, callback)
{
    connect(function (err, client, done)
    {
        if (err) return callback(err);

        function rollback(err)
        {
            client.query('ROLLBACK', done);
            callback(err);
        }

        client.query('BEGIN', function (err) {
            if (err)
                return rollback(err);

            runner(client, function(err, data) {
                if (err)
                    return rollback(err);

                client.query('COMMIT', function (err) {
                    if (err)
                        return rollback(err);

                    done();
                    callback(null, data);
                });
            });
        });
    });
}

// callback is called with (err, client, done)
exports.getClient = function(callback) {
    var client = new pg.Client(databaseUrl);

    client.connect(function(err) {
        if (err) return callback(err);

        callback(null, client);
    });
};


/**********************************************************************
 * get last block from blocks table
 * @param callback
 */
exports.getLastBlock = function(callback)
{
    query('SELECT * FROM eth_blocks ORDER BY height DESC LIMIT 1', function(err, results)
    {
        if (err)
        {// db error
            return callback(err);
        }

        if (results.rows.length === 0)
        {
            return callback(null, { height: 0, hash: '0x41941023680923e0fe4d74a34bdac8141f2540e3ae90623718e47d66d1ca4a2d' }); // genesis block
        }

        assert(results.rows.length === 1);// check LIMIT 1 : db error
        callback(null, results.rows[0]); // int4:height  text:hash from db:blocks
    });
};

exports.getBlock = function(height, callback) {
    query('SELECT * FROM eth_blocks WHERE height = $1', [height], function(err, results) {
        if (err) return callback(err);

        if (results.rows.length === 0)
            return callback(new Error('Could not find block ' + height));

        callback(null, results.rows[0]);
    });
};

exports.insertBlock = function(height, hash, callback) {
    query('INSERT INTO eth_blocks(height, hash) VALUES($1, $2)', [height, hash], callback);
};

function addDeposit(userId, txid, amount_finney, callback)
{
    // userId = depositAddresses[userId];
    assert(typeof amount_finney === 'number');
   // Amount is in bitcoins...
    amount_satoshi = parseInt(amount_finney * g_fEBRate * 1e5);
    
    getClient(function(client, callback)
    {
        async.parallel([
            function(callback)
            {
                client.query('INSERT INTO withdrawals(user_id, amount, txid, side, market, created_at) ' +
                    "VALUES($1, $2, $3, 'Deposit', 'ETH', $4)",
                    [userId, amount_finney/1000, txid, today], callback);

                // var max;
                // client.query('SELECT MAX(id) FROM fundings', function(err, res){
                //     if (err) return callback(err);
                //     max = res.rows[0].max;
                //     if (max == null) max = 0;
                //     client.query('INSERT INTO fundings(id, user_id, amount, deposit_txid, description, baseunit, currency) ' +
                //               "VALUES(" + (max + 1) + ", $1, $2, $3, 'ETH Deposit', $4, 'ETH')",
                //               [userId, amount_satoshi, txid, amount_finney / 1000], callback);
                // })
                console.log("Deposit of ETH is successful");
            },
            function(callback)
            {
                 client.query("UPDATE users SET balance_eth = balance_eth + $1 WHERE id = $2",
                              [amount_finney/1000, userId], callback);
                 console.log("update user's table for eth")
            }],
            callback);
    },function(err)
    {
        if (err)
        {
            if (err.code == '23505')
            { // constraint violation
                console.log('Warning deposit constraint violation for (', userId, ',', txid, ')');
                return callback(null);
            }

            console.log('[INTERNAL_ERROR] could not save: (', userId, ',', txid, ') got err: ', err);
            return callback(err);
        }

        return callback(null);
    });

    console.log('Deposit successful from user id ', userId, ' with ', amount_finney, 'finney');
};

// exports.processTransaction = function(to, values, hashes, callback)
// {
//     // console.log('transaction counts: ' + values.length + ' ----- amounts transfered counts: ' + values.length);
//     var result = [];

//     for (var i = 0; i < values.length; i++)
//     {
//         var userId;
        
//         var val = to[i];
//         if (keys.includes(val)) {
//             userId = i;
//             // var info = web3.eth.getTransaction(to[i]);
//             var amount = values[i];
//             var key = to[i];
//             var item = {};
//             item['userid'] = key;
//             item['amount'] = amount
//             item['txid'] = hashes[i];
//             result.push(item);
//         }
//     }
//     return callback(null, result);
// }

exports.checkUserId = function(from, value, hashes, callback){
    var result = {};
    query('SELECT user_id FROM eth_deposit_src WHERE eth_addr = $1', [from], function(err, res){
        if (err) return callback(err);
        if (err == null && res.rows.length == 1) {
            var userid = res.rows[0].user_id;
            addDeposit(userid, hashes, value, function(err, res){
                return callback(err);
            });
        }
    })
}

////// ETH / BTC
exports.updateETHvsBTCRate = function(fRate, callback)
{
    g_fEBRate = fRate;

    query('SELECT strvalue FROM common WHERE strkey = $1', ['fEBRate'], function(err, results)
    {
        if (err) return callback(err);

        if (results.rows.length === 0)
        {
            query('SELECT count(*) AS cnt FROM common', [], function(err, results)
            {
                if (err) return callback(err);

                var nCnt = results.rows[0].cnt;
                query('INSERT INTO common(nId, strkey, strvalue) VALUES($1, $2, $3)', [nCnt + 1, 'fEBRate', fRate], function(err)
                {
                    if (err) return callback(err);
                    return callback('success');
                });
            });
        }
        else if (results.rows.length === 1)
        {
            query('UPDATE common SET strvalue = $1 WHERE strkey = $2', [fRate, 'fEBRate'], function (err)
            {
                if (err) return callback(err);
                return callback('success');
            });
        }
        else
        {
            return callback('error');
        }
    });
};

exports.LoadDepositSrc = function(callback) {

    var addr, pass;
    var retval = {};

    var sql = "SELECT strvalue FROM common WHERE strkey='ethereum_deposit_addr'";
    query(sql, function(error, result){
        if (error) return callback(error);
        addr = result.rows[0].strvalue;

        sql = "SELECT strvalue FROM common WHERE strkey='ethereum_deposit_pass'";
        query(sql, function(err, res){
            if(err) return callback(err);
            pass = res.rows[0].strvalue;

            retval['addr'] = addr;
            retval['pass'] = pass;
            return callback(null, retval);
        });
    });
};
